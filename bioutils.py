from Bio import SeqIO
from Bio.SeqUtils import gc_fraction

def parse_fasta(file_path):
    sequences = {}
    current_header = None
    with open(file_path, 'r') as file:
        for line in file:
            line = line.strip()
            if line.startswith('>'):
                current_header = line[1:]
                sequences[current_header] = ''
            elif current_header is not None:
                sequences[current_header] += line
    return sequences

def transcribe_dna_to_rna(dna_sequence):
    return dna_sequence.replace('T', 'U')

def calculate_gc_percentage(dna_sequence):
    gc_count = dna_sequence.count('G') + dna_sequence.count('C')
    total_bases = len(dna_sequence)
    gc_percentage = (gc_count / total_bases) * 100
    return gc_percentage

def is_ambiguous_dna(dna_sequence):
    ambiguous_bases = set('BDHKMNRSVWY')
    return any(base in ambiguous_bases for base in dna_sequence)

def get_genbank_info(accession):
    genbank_file = f'data/{accession}.genbank'
    print(genbank_file)
    try:
        records = SeqIO.parse(genbank_file, 'genbank')
        info = []
        
        for record in records:
            record_info = {
                'title': record.description,
                'accession': record.id,
                'organism': record.annotations.get('organism', ''),
                'ncbi_link': f'https://www.ncbi.nlm.nih.gov/nuccore/{record.id}',
                'latest_reference': record.annotations.get('references', [])[0].title,
                'num_features': len(record.features),
                'cds_info': [{'type': feature.type, 'location': str(feature.location)} for feature in record.features if feature.type == 'CDS'],
                'sequence': str(record.seq)[:90],
                'gc': round(gc_fraction(record.seq)),
                'translation_first_30_aa': str(record.seq.translate(to_stop=True))[:30]
            }
            info.append(record_info)

        for i in record.features:
            print(i)
        return info
    except FileNotFoundError:
        return {'error': f'No es troba cap fitxer genbank amb l\'accession {accession}.'}
