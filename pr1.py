# Problema 1 - Mol·lècules (2 punts)

# Volem mostrar per pantalla el nom i la URL de les 4 macromol·lecules de l’ADN; així podrem
# descarregar-les des del Pubchem=PCCompound en un futur (podeu trobar-la dins del codi).
# Pista: El format de la URL és el següent, on ${name} és el nom de la molècula (pex Adenine)
# https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/name/${name}/SDF

# També volem generar la URL de la wikipedia de cada una per a poder fer web scrapping en un futur.
# Com a pista, aquí teniu la URL de l’Adenina: https://en.wikipedia.org/wiki/Adenine
# Hem d’assegurar-nos d’informar a l’usuari de possibles errors, i assegurar-nos que només es
# descarregaran els fitxers que no existeixin al disc

# Definir les 4 macromol·lecules de l'ADN i les seves URL de Wikipedia
molecules = {
    'Adenine': 'https://en.wikipedia.org/wiki/Adenine',
    'Thymine': 'https://en.wikipedia.org/wiki/Thymine',
    'Guanine': 'https://en.wikipedia.org/wiki/Guanine',
    'Cytosine': 'https://en.wikipedia.org/wiki/Cytosine'
}

for molecule, wikipedia_url in molecules.items():
    pubchem_url = f'https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/name/{molecule}/SDF'
    print(f'URL de PubChem per {molecule}: {pubchem_url}')

    print(f'URL de Wikipedia per {molecule}: {wikipedia_url}')

    print('-' * 50)