from flask import Flask, request, jsonify
import bioutils

app = Flask(__name__, static_folder="out", static_url_path="/")


@app.route('/api/genbank_info/<accession>', methods=['GET'])
def genbank_info_get(accession):
    if request.method == 'GET':
        info = bioutils.get_genbank_info(accession)
        return jsonify(info)


@app.route('/api/genbank_info', methods=['POST'])
def genbank_info_post():
    if request.method == 'POST':
        data = request.get_json()
        if 'accession' in data:
            sequence = data['accession']
            info = bioutils.get_genbank_info(sequence)
            return jsonify(info)
        else:
            return jsonify({'error': 'No se ha proporcionado la secuencia.'})

if __name__ == "__main__":
    app.run(debug=True)
