from Bio import Entrez, SeqIO
import os

def download_crt_records():
    Entrez.email = "mamoro1@xtec.cat"

    gene_query = 'CRT[Gene Name] AND "Plasmodium falciparum"[Organism]'
    search_handle = Entrez.esearch(db="nucleotide", term=gene_query, retmax=30)
    search_results = Entrez.read(search_handle)
    search_handle.close()

    if 'IdList' in search_results and search_results['IdList']:
        gene_ids = search_results['IdList']

        id_filename = "data/pr4/downloaded_ids.txt"
        downloaded_ids = set()

        if os.path.exists(id_filename):
            with open(id_filename, "r") as id_file:
                downloaded_ids = set(id_file.read().splitlines())

        for gene_id in gene_ids:
            if gene_id not in downloaded_ids:
                download_handle = Entrez.efetch(db='nucleotide', id=gene_id, rettype='gb', retmode='text')
                record = SeqIO.read(download_handle, 'genbank')
                download_handle.close()

                print(f"ID: {record.id}")
                print(f"Descripción: {record.description}")

                genbank_filename = f"data/pr4/{record.id}.gbk"
                SeqIO.write(record, genbank_filename, 'genbank')
                print(f"Archivo GenBank guardado: {genbank_filename}")

                fasta_filename = f"data/pr4/{record.id}.fasta"
                SeqIO.write(record, fasta_filename, 'fasta')
                print(f"Archivo FASTA guardado: {fasta_filename}")

                downloaded_ids.add(gene_id)

        with open(id_filename, "w") as id_file:
            id_file.write("\n".join(downloaded_ids))

    else:
        print("No se encontraron registros para el gen CRT en Plasmodium falciparum.")

if __name__ == "__main__":
    download_crt_records()
