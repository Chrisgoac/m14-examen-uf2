# Crea un programa que llegeixi un fitxer .fasta (pot ser unifasta o multifasta) i que realitzi les següents
# operacions per a cada seqüència:
# 1. Mostrar la capçalera
# 2. Mostrar la seqüència d’ADN
# 3. Transcriure l’ARN.
# 4. La longitud total de la cadena.
# 5. El porcentatge gc (bases G i C respecte el total)
# 6. Mostri si es tracta d’un ADN ambigu o no (els ADN ambigus tenen unes lletres especials fora de la
# A,C,G,T).

import bioutils

def analyze_sequence(header, dna_sequence):
    print('-' * 50)
    print(f'Capçalera: {header}')
    print('-' * 50)
    print(f'Seqüència d\'ADN: {dna_sequence}')
    print('-' * 50)
    

    rna_sequence = bioutils.transcribe_dna_to_rna(dna_sequence)
    print(f'Seqüència d\'ARN: {rna_sequence}')
    print('-' * 50)

    total_length = len(dna_sequence)
    print(f'Longitud total de la cadena: {total_length} bases')
    print('-' * 50)

    gc_percentage = bioutils.calculate_gc_percentage(dna_sequence)
    print(f'Percentatge GC: {gc_percentage:.2f}%')
    print('-' * 50)

    is_ambiguous = bioutils.is_ambiguous_dna(dna_sequence)
    print(f'És un ADN ambigu? {"Sí" if is_ambiguous else "No"}')
    print('-' * 50)

def main(file_path):
    sequences = bioutils.parse_fasta(file_path)
    for header, sequence in sequences.items():
        analyze_sequence(header, sequence)

if __name__ == "__main__":
    file_path = "data/fastafile.fasta"
    main(file_path)

    print()
    
    file_path = "data/fastafileambi.fasta"
    main(file_path)
